module.exports = {
    branches: ["master"],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        ['@semantic-release/git', {
            assets: ["CHANGELOG.md", "Cargo.toml", "Cargo.lock"],
        }],
    ],
};
