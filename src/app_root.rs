use stylist::style;
use stylist::yew::styled_component;
use yew::prelude::*;

use crate::components::atoms::main_title::MainTitle;

#[styled_component(App)]
pub fn app() -> Html {
    let stylesheet = style!("").unwrap();
    html! {
        <div class={stylesheet}>
            <MainTitle title={"Hello World"}/>
        </div>
    }
}
