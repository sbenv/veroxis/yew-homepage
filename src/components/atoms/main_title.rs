use stylist::yew::styled_component;
use stylist::Style;
use yew::prelude::*;

const STYLESHEET: &str = include_str!("main_title.css");

#[derive(Properties, PartialEq, Eq)]
pub struct Props {
    pub title: String,
}

#[styled_component(MainTitle)]
pub fn main_title(props: &Props) -> Html {
    let stylesheet: Style = Style::new(STYLESHEET).expect("failed to parse CSS");
    html! {
        <div class={stylesheet}>
            <h1>{&props.title}</h1>
        </div>
    }
}
