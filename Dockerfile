FROM nginxinc/nginx-unprivileged:1.27.2-alpine

COPY default.conf.template /etc/nginx/templates/default.conf.template
COPY dist /app

ENV NGINX_PORT=8080
