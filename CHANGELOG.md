## [1.6.3](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.6.2...v1.6.3) (10/7/2023)


### Bug Fixes

* updates ([36b245c](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/36b245c5547d16c835ef89e7cca93e516b1655d2))

## [1.6.2](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.6.1...v1.6.2) (2023-06-11)


### Bug Fixes

* adapt to regression in linker ([faa028a](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/faa028a05af4b2ccc574c15feb714cfb32a9ff5e))

## [1.6.1](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.6.0...v1.6.1) (2022-11-26)


### Bug Fixes

* upgrade breaking changes in yew and stylist ([09d5115](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/09d511539a72ce28d97c6ec681eead9147dab63f))

# [1.6.0](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.5.0...v1.6.0) (2022-10-09)


### Features

* set default port to 8080 ([96ec66f](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/96ec66fee94a72a859c48badd10706c20f90c1ab))

# [1.5.0](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.4.2...v1.5.0) (2022-09-21)


### Features

* **nginx:** remove server_name directive ([a027488](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/a0274887c171ca38c941826717a88b8d810b1bac))

## [1.4.2](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.4.1...v1.4.2) (2022-06-12)


### Bug Fixes

* **deps:** update rust crate stylist to 0.10.1 ([253f0cc](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/253f0cc14083f3d8a1983a9c18109bb541370426))

## [1.4.1](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.4.0...v1.4.1) (2022-06-08)


### Bug Fixes

* **deps:** update rust crate tracing to 0.1.35 ([2019a1b](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/2019a1b935668680d19534667bccf74151808296))

# [1.4.0](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.3.0...v1.4.0) (2022-06-06)


### Features

* various updates ([8116603](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/8116603cb1b72c03855c60cc5ce6c633a8320041))

# [1.3.0](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.2.1...v1.3.0) (2022-06-03)


### Features

* print app version to web console ([718c522](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/718c5226950fa84d00cfc52c0e9e633312ae2dab))

## [1.2.1](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.2.0...v1.2.1) (2022-06-02)


### Bug Fixes

* **deps:** use versioned nginx image ([dc20302](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/dc203029cfff3de7705f53d4ce5fff41a4296274))

# [1.2.0](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.1.0...v1.2.0) (2022-05-28)


### Features

* create yew sample app ([67b390a](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/67b390a1fd99ec01299ddbf7f445826c67825bf6))

# [1.1.0](https://gitlab.com/sbenv/veroxis/yew-homepage/compare/v1.0.0...v1.1.0) (2022-05-28)


### Features

* add rust build in pipeline ([479102e](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/479102e28440ef2f13f7e204567003c528418545))
* change image to nginx-unprivileged ([847499e](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/847499e9c7e21b65ebd88eeb450cc18961153a0b))

# 1.0.0 (2022-05-28)


### Bug Fixes

* add releaserc file ([1064307](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/1064307288f52ceff46377794fc04b148908b0ce))


### Features

* initial commit ([d21017f](https://gitlab.com/sbenv/veroxis/yew-homepage/commit/d21017f068cc2a472950f6797cb1ce634149acca))
